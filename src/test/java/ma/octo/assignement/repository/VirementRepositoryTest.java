package ma.octo.assignement.repository;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.service.VersementService;

import java.math.BigDecimal;
import java.util.List;

import javax.transaction.Transactional;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class VirementRepositoryTest {

  @Autowired
  private VersementRepository repVersement;

  @Autowired 
  private VersementService versementService;
  @Test
  public void findOne() throws Exception {
	  assert(versementService.getAll().size()>0);
  }

  @Test
  public void findAll() throws Exception {
	  List<Versement> versements = versementService.getAll();
	  assert(versements.get(0).getNameEmetteur().equals("Le Emmeter nom"));
  }

  @Test
  public void save() throws Exception {
	  List<Versement> versements = versementService.getAll();
	  int size = versements.size();
	  Versement v = versements.get(0);
	  v.setId(v.getId().longValue()+10);
	  v.setNameEmetteur("None sens");
	  v.setMontantTransfer(new BigDecimal(5000));
	  versementService.add(v);
	  assert(versementService.getAll().size() == (size +1) );
  }

  @Test
  public void delete() throws Exception {
	  versementService.getAll().forEach((vers)->{
		  try {
			versementService.delete(vers.getId());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  });
	  assert(versementService.getAll().size() == 0);
	  
  }
}